/* regions.c */

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct point {
    double x;
    double y;
};

int G = 1;

int main(int argc, char *argv[]) {
    int            i = 0;
    int          a[] = {4, 6, 6, 3, 7};
    char          *s = "pgui";
    struct point   p = {0, 0};
    struct point  *q = NULL;
    struct point  *x = malloc(sizeof(struct point));
    struct point  *y = malloc(10*sizeof(struct point));
    struct point **z = malloc(10*sizeof(struct point *));
    static int	   t = 2;

    printf("  i. Address: 0x%012lx, Size: %lu\n", (size_t)&i, sizeof(i));
    printf("  a. Address: 0x%012lx, Size: %lu\n", (size_t)&a, sizeof(a));
    printf("  s. Address: 0x%012lx, Size: %lu\n", (size_t)&s, sizeof(s));
    printf(" *s. Address: 0x%012lx, Size: %lu\n", (size_t) s, strlen(s) + 1);
    printf("  p. Address: 0x%012lx, Size: %lu\n", (size_t)&p, sizeof(p));
    printf("  q. Address: 0x%012lx, Size: %lu\n", (size_t)&q, sizeof(q));
    printf("  x. Address: 0x%012lx, Size: %lu\n", (size_t)&x, sizeof(x));
    printf(" *x. Address: 0x%012lx, Size: %lu\n", (size_t) x, sizeof(*x));
    printf("  y. Address: 0x%012lx, Size: %lu\n", (size_t)&x, sizeof(y));
    printf(" *y. Address: 0x%012lx, Size: %lu\n", (size_t) y, sizeof(*y));
    printf("  z. Address: 0x%012lx, Size: %lu\n", (size_t)&x, sizeof(z));
    printf(" *z. Address: 0x%012lx, Size: %lu\n", (size_t) z, sizeof(*z));
    printf("  G. Address: 0x%012lx, Size: %lu\n", (size_t)&G, sizeof(G));
    printf("  t. Address: 0x%012lx, Size: %lu\n", (size_t)&t, sizeof(t));

    /* Demonstrations
     *
     * - Segfault if we do `s[1] = 'b'`.  No segfault if we change *s to s[].
     *	    
     *	    Arrays are LIKE pointers, are not actually pointers (can't do
     *	    assignment, don't take up their own space).
     *
     * - Running repeatedly will show that the stack and heap allocations
     *   change, but not the code and data variables do not.
     *
     */

    return EXIT_SUCCESS;
}
