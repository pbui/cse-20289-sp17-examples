/* rpn.c */

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Node structure ------------------------------------------------------------*/

struct node {
    int		 value;
    struct node *next;
};

struct node * 	node_create(int value, struct node *next) {
    // Allocate struct node and set fields
    struct node *n = malloc(sizeof(struct node));
    if (n) {
	n->value = value;
	n->next	 = next;
    }
    return n;
}

void 		node_delete(struct node *n) {
    // Release previously allocated struct node
    free(n);
}

/* List structure ------------------------------------------------------------*/

struct list {
    struct node *head;
};

struct list * 	list_create() {
    // Allocate zero'd struct list
    return calloc(1, sizeof(struct node));
}

void 		list_delete(struct list *l) {
    // Release previously allocated list node and children
    struct node *next = NULL;
    for (struct node *curr = l->head; curr; curr = next) {
	next = curr->next;
	free(curr);
    }
    free(l);
}

size_t		list_size(struct list *l) {
    // Iterate through list to compute size
    size_t size = 0;
    for (struct node *curr = l->head; curr; curr = curr->next)
	size++;
    return size;
}

void		list_dump(struct list *l, FILE *stream) {
    // Iterate through list and print out each node
    for (struct node *curr = l->head; curr; curr = curr->next) {
	fprintf(stream, "Node{%d, 0x%lx}\n",
		curr->value, (uintptr_t)curr->next);
    }
}

void 		list_push_front(struct list *l, int value) {
    // Create new node and add it to the front of the list
    l->head = node_create(value, l->head);
}

int		list_pop_front(struct list *l) {
    // Remove front of list and return value
    struct node *top  = l->head;
    int         value = 0;
    if (top) {
	value   = top->value;
	l->head = top->next;
	free(top);
    }
    return value;
}


/* Main execution ------------------------------------------------------------*/

int main(int argc, char *argv[]) {
    char buffer[BUFSIZ];

    while (fgets(buffer, BUFSIZ, stdin)) {
	struct list *stack = list_create();

	fputs(buffer, stdout);

	for (char *token = strtok(buffer, " \t\n"); token; token = strtok(NULL, " \t\n")) {
    	    int op1, op2;

    	    switch (token[0]) {
		case '+':
		    op1 = list_pop_front(stack);
		    op2 = list_pop_front(stack);
		    list_push_front(stack, op2 + op1);
		    break;
		case '-':
		    op1 = list_pop_front(stack);
		    op2 = list_pop_front(stack);
		    list_push_front(stack, op2 - op1);
		    break;
		case '*':
		    op1 = list_pop_front(stack);
		    op2 = list_pop_front(stack);
		    list_push_front(stack, op2 * op1);
		    break;
		default:
		    list_push_front(stack, atoi(token));
		    break;
	    }
	}

	printf("%d\n", stack->head->value);
	list_delete(stack);
    }

    return EXIT_SUCCESS;
}
