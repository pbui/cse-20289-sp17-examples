#!/usr/bin/env python2.7

# Variant 1: Prompt user for data           (I/O)
# Variant 2: Read data from file            (I/O)
# Varient 3: Save translation to output     (I/O)

import string
import sys

ROTATION = 13

if len(sys.argv) == 2:
    ROTATION = int(sys.argv[1]) % 26

SOURCE_LOWER = string.ascii_lowercase
SOURCE_UPPER = string.ascii_uppercase
SOURCE_ALL   = SOURCE_LOWER + SOURCE_UPPER

TARGET_LOWER = SOURCE_LOWER[ROTATION:] + SOURCE_LOWER[:ROTATION]
TARGET_UPPER = SOURCE_UPPER[ROTATION:] + SOURCE_UPPER[:ROTATION]
TARGET_ALL   = TARGET_LOWER + TARGET_UPPER

TRANSLATION  = dict(zip(SOURCE_ALL, TARGET_ALL))

for line in sys.stdin:
    line   = line.rstrip()
    result = ''
    for letter in line:
        result += TRANSLATION.get(letter, letter)

    print result
