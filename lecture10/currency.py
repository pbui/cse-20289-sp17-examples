#!/usr/bin/env python2.7

# Variant 1: read command line argument and convert

import requests

url    = 'http://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote?format=json'
result = requests.get(url)
data   = result.json()
rows   = []

for resource in data['list']['resources']:
    fields = resource['resource']['fields']
    if not 'name' in fields:
        continue

    rows.append((fields['name'].split('/')[-1], float(fields['price'])))

for name, price in sorted(rows):
    print '{:>20} {:>12.4f}'.format(name, price)
