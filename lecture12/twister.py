#!/usr/bin/env python2.7

import sys
import itertools

def odds():
    n = 1
    while True:
        yield n
        n += 2

def evens():
    n = 2
    while True:
        yield n
        n += 2

# Variant 1

og = odds()
eg = evens()

for i in range(int(sys.argv[1])):
    print 'Odds:  ', next(og)
    print 'Evens: ', next(eg)

# Variant 2

for i, (o, e) in enumerate(itertools.izip(odds(), evens())):
    if i == int(sys.argv[1]):
        break

    print 'Odds:  ', o
    print 'Evens: ', e
