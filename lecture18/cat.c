/* cat.c */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    int fd = open(argv[1], O_RDONLY);
    if (fd < 0) {
    	fprintf(stderr, "Unable to open %s: %s\n", argv[1], strerror(errno));
    	return EXIT_FAILURE;
    }

    char buffer[BUFSIZ];
    int  nread;

    lseek(fd, 10, SEEK_SET);    // Skip first 10 bytes

    while ((nread = read(fd, buffer, BUFSIZ)) > 0) {
        write(STDOUT_FILENO, buffer, nread);
    }
    
    lseek(fd, 0, SEEK_SET);     // Rewind the stream
    
    while ((nread = read(fd, buffer, BUFSIZ)) > 0) {
        write(STDOUT_FILENO, buffer, nread);
    }

    close(fd);
    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
