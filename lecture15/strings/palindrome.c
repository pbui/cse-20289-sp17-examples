/* palindrome.c */

#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

bool is_palindrome(const char *s) {
    const char *front = s;
    const char *back  = s + strlen(s) - 1;

    while (front <= back && *front == *back) {
    	front++;
    	back--;
    }

    return (front > back);
}

int main(int argc, char *argv[]) {
    char buffer[BUFSIZ];

    while (fgets(buffer, BUFSIZ, stdin)) {
    	buffer[strlen(buffer) - 1] = 0;
    	printf("%s is %spalindrome!\n", buffer, is_palindrome(buffer) ? "" : "not ");
    }

    return EXIT_SUCCESS;
}
