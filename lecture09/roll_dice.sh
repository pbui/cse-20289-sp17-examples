#!/bin/sh

MIN=1
MAX=6
NUM=10

usage() {
    cat <<EOF
usage: $(basename $0) [-r ROLLS -s sides]

-r ROLLS	Number of rolls of die (default: $NUM)
-s SIDES	Number of sides on die (default: $MAX)
EOF
    exit $1
}

while [ $# -gt 0 ]; do
    case $option in
    r)	NUM=$2 shift;;
    s)	MAX=$2 shift;;
    h)	usage 0	    ;;
    *)	usage 1	    ;;
    esac
    shift
done

for i in $(seq $NUM); do
    shuf -i $MIN-$MAX -n 1
done
